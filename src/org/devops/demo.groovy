package org.devops

// define GetUserName
def GetUserNameByID(id,users){
	for (i in users){
		if (i["id"] == id){
			return i["name"]
		}
	}
	return "null"
}

def GetUserIDByName(name,users){
	for (i in users){
		if (i["name"] == name){
			return i["id"]
		}
	}
	return "null"
}
